package com.practitioner.services;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.practitioner.model.Usuario;

@Service
public class UsuarioServices {

	private final AtomicLong SECUENCIADOR = new AtomicLong(0);
	private final ConcurrentHashMap<Long, Usuario> USUARIOS = new ConcurrentHashMap<>();

	public List<Usuario> list() {
		return this.USUARIOS.entrySet().stream().map(Entry::getValue).collect(Collectors.toList());
	}

	public Usuario get(long id) {
		return this.USUARIOS.get(id);
	}

	public Long add(Usuario u) {
		Long id = this.SECUENCIADOR.incrementAndGet();
		u.setId(id);
		this.USUARIOS.put(id, u);
		return id;
	}

	public void update(long id, Usuario u) {
		u.setId(id);
		this.USUARIOS.put(id, u);
	}

	public void delete(long id) {
		this.USUARIOS.remove(id);
	}

}
