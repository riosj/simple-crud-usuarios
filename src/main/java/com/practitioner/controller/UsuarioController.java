package com.practitioner.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practitioner.dto.UsuarioDTO;
import com.practitioner.model.Usuario;
import com.practitioner.services.UsuarioServices;

@RestController
@RequestMapping("usuarios")
public class UsuarioController {

	@Autowired
	UsuarioServices usuarioServices;

	@GetMapping
	public ResponseEntity<List<Usuario>> list() {
		return ResponseEntity.ok(usuarioServices.list());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Usuario> get(@PathVariable long id) {
		Usuario usuario = usuarioServices.get(id);
		if (usuario == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.ok(usuario);
	}

	@PostMapping
	public ResponseEntity<String> add(@RequestBody Usuario u) {
		usuarioServices.add(u);
		return ResponseEntity.status(HttpStatus.CREATED).body("Usuario creado");
	}

	@PutMapping("/{id}")
	public ResponseEntity update(@PathVariable int id, @RequestBody Usuario usuario) {
		Usuario u = usuarioServices.get(id);
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		usuarioServices.update(id, usuario);
		return ResponseEntity.ok("Producto actualizado");
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Integer id) {
		Usuario u = usuarioServices.get(id);
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		usuarioServices.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PatchMapping("/{id}")
	public ResponseEntity patch(@RequestBody UsuarioDTO usuario, @PathVariable int id) {
		Usuario u = usuarioServices.get(id);
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		if (ObjectUtils.isEmpty(usuario.getNombres())) {
			u.setNombres(usuario.getNombres());
		}
		if (ObjectUtils.isEmpty(usuario.getApellidos())) {
			u.setApellidos(usuario.getApellidos());
		}
		if (usuario.getEdad() > 0) {
			u.setEdad(usuario.getEdad());
		}
		usuarioServices.update(id, u);
		return ResponseEntity.ok(u);
	}

	@GetMapping("/{id}/productos")
	public ResponseEntity getUsuariosIdProducto(@PathVariable int id) {
		Usuario u = usuarioServices.get(id);
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		if (u.getProductos() != null) {
			return ResponseEntity.ok(u.getProductos());
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
